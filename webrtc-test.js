//import { Strophe } from "../src/core";

var DOMAIN_NAME = 'stg-xmpp.fconnect.com.vn';
var BOSH_SERVICE = 'https://' + DOMAIN_NAME + ':5443/bosh';
var WS_SERVICE = 'wss://' + DOMAIN_NAME + ':5443/ws';
var connection = null;
var connection_status = "";
var disconnectIsClicked = false;
var isCaller = false;

var localJID = "";
var localPass = "12345678a@";
var remoteJID = "";

var myPeerConnection = null;    // RTCPeerConnection
var transceiver = null;         // RTCRtpTransceiver
var webcamStream = null;        // MediaStream from webcam

let VIDEO_NAMESPACE = "com.webrtc.video";
let VIDEO_ELEMENT = "video-chat";
let VIDEO_TYPE_ATTR_NAME = "type";

let SDP_NAMESPACE = "com.webrtc.sdp";
let SDP_ELEMENT = "sdp";
let SDP_DESC_ATTR_NAME = "description";
let SDP_TYPE_ATTR_NAME = "type";

let ICE_NAMESPACE = "com.webrtc.ice_candidate";
let ICE_ELEMENT = "ice_candidate";
let sdpMidText_ATTR_NAME = "sdpMidText";
let sdpMLineIndexText_ATTR_NAME = "sdpMLineIndexText";
let sdpText_ATTR_NAME = "sdpText";

function log(text) 
{
    $('#log').append('<div></div>').append(document.createTextNode(text));

    var time = new Date();
    console.log("[" + time.toLocaleTimeString() + "] " + text);

}

function log_error(text) {
    var time = new Date();
  
    console.trace("[" + time.toLocaleTimeString() + "] " + text);
  }



function create_video_invitation(to, from ){
    var reply = $msg({to: to, from: from, type: 'chat'})
    .c(VIDEO_ELEMENT, {xmlns: VIDEO_NAMESPACE, type: "video-invitation"})
    // log(reply.toString())
    return reply
}

function create_video_accept(to, from ){
    var reply = $msg({to: to, from: from, type: 'chat'})
    .c(VIDEO_ELEMENT, {xmlns: VIDEO_NAMESPACE, type: "video-agree"})
    log(reply.toString())
    return reply
}

function create_video_deny(to, from ){
    var reply = $msg({to: to, from: from, type: 'chat'})
    .c(VIDEO_ELEMENT, {xmlns: VIDEO_NAMESPACE, type: "video-deny"})
    // log(reply.toString())
    return reply
}

function create_video_end(to, from ){
    var reply = $msg({to: to, from: from, type: 'chat'})
    .c(VIDEO_ELEMENT, {xmlns: VIDEO_NAMESPACE, type: "video-ended"})
    // log(reply.toString())
    return reply
}


function create_sdp_offer(to, from, sdp_desc){
    var reply = $msg({to: to, from: from, type: 'chat'})
    .c(SDP_ELEMENT, {xmlns: SDP_NAMESPACE, type: "offer"}, sdp_desc)
    // log(reply.toString())
    return reply
}

function create_sdp_answer(to, from, sdp_desc){
    var reply = $msg({to: to, from: from, type: 'chat'})
    .c(SDP_ELEMENT, {xmlns: SDP_NAMESPACE, type: "answer"}, sdp_desc)
    // log(reply.toString())
    return reply
}

function create_ice_candidate(to, from, candidate){
    let sdpMidText = candidate.sdpMid;
    let sdpMLineIndex = candidate.sdpMLineIndex;
    let sdpText = candidate.candidate;
    var reply = $msg({to: to, from: from, type: 'chat'})
    .c(ICE_ELEMENT, {xmlns: ICE_NAMESPACE, 
        sdpMidText: sdpMidText, 
        sdpMLineIndexText: sdpMLineIndex,
     }, sdpText)
    // log(reply.toString())
    return reply
}


function onConnect(status)
{
    if (status == Strophe.Status.CONNECTING) {
        log('Strophe is connecting.');
    } else if (status == Strophe.Status.CONNFAIL) {
        log('Strophe failed to connect.');
	    $('#connect').get(0).value = 'Connect';
    } else if (status == Strophe.Status.DISCONNECTING) {
        log('Strophe is disconnecting.');
    } else if (status == Strophe.Status.DISCONNECTED) {
        log('Strophe is disconnected.');
	    $('#connect').get(0).value = 'Connect';
	    connection_status = "disconnected";
    } else if (status == Strophe.Status.CONNECTED) {
        log('Strophe is connected.');
        log('ECHOBOT: Send a message to ' + connection.jid + 
	    ' to talk to me.');
	    connection_status = "connected";
	    connection.addHandler(onMessage, null, 'message', null, null,  null); 
	    connection.send($pres().tree());
    }
}

var mediaConstraints = {
    audio: true, // We want an audio track
    video: true // ...and we want a video track
  };


function onMessage(msg) {
    var to = msg.getAttribute('to');
    var from = msg.getAttribute('from');
    var msg_type = msg.getAttribute('type');

    var video_chat_elems = msg.getElementsByTagName(VIDEO_ELEMENT);
    if (video_chat_elems[0] !== undefined)
    {
        console.log(video_chat_elems[0])
        var namespace = video_chat_elems[0].namespaceURI
        if(namespace === VIDEO_NAMESPACE)
        {
            remoteJID = from
            var video_type = video_chat_elems[0].getAttribute(VIDEO_TYPE_ATTR_NAME);

            log(video_type)
            if(video_type === "video-invitation")
            {
                isCaller = false;
                let r = create_video_accept(from, to);
                connection.send(r.tree());
            }
            if(video_type === "video-agree")
            {
                isCaller = true;
                if (myPeerConnection) {
                  alert("You can't start a call because you already have one open!");
                } else {
                  if (!document.getElementById('screen').checked) {
                    createPeerConnection();
                    navigator.mediaDevices.getUserMedia(mediaConstraints)
                    .then(function(localStream) {
                      document.getElementById("local_video").srcObject = localStream;
                      localStream.getTracks().forEach(track => myPeerConnection.addTrack(track, localStream));
                    })
                    .catch(handleGetUserMediaError);
                  }
                  else
                  {
                    createPeerConnection();
                    localStream = document.getElementById("local_video").srcObject;
                    localStream.getTracks().forEach(track => myPeerConnection.addTrack(track, localStream));
                  }
                }
                // try {
                //   log("---> Creating offer");
                //   const offer = myPeerConnection.createOffer();
              
                //   // Establish the offer as the local peer's current
                //   // description.
              
                //   log("---> Setting local description to the offer");
                //   await myPeerConnection.setLocalDescription(offer);
              
                //   // Send the offer to the remote peer.
              
                //   log("---> Sending the offer to the remote peer");
            
                //   let r = create_sdp_offer(remoteJID, localJID, myPeerConnection.localDescription.sdp);
                //   connection.send(r.tree());
            
                // } catch(err) {
                //   log("*** The following error occurred while handling the negotiationneeded event:");
                //   reportError(err);
                // };

            }
            if(video_type === "video-deny")
            {

            }
            if(video_type === "video-ended")
            {
                isCaller = false;
                hangUpCall();
            }
        }
        

    }

    var sdp_elems = msg.getElementsByTagName(SDP_ELEMENT);
    if (sdp_elems[0] !== undefined)
    {
        // console.log(sdp_elems[0]);
        var namespace = sdp_elems[0].namespaceURI
        if(namespace === SDP_NAMESPACE)
        {
            remoteJID = from
            var sdp_type = sdp_elems[0].getAttribute(SDP_TYPE_ATTR_NAME);
            var sdp_desc = Strophe.getText(sdp_elems[0]);
            // console.log(sdp_type);
            // console.log(sdp_desc);
            if(sdp_type === "offer")
            {
                handleVideoOfferMsg(from, to, sdp_desc);
            }
            if(sdp_type === "answer")
            {
                handleVideoAnswerMsg(from, to, sdp_desc);
            }
        }
        

    }

    var ice_candidate_elems = msg.getElementsByTagName(ICE_ELEMENT);
    if (ice_candidate_elems[0] !== undefined)
    {
        // console.log(ice_candidate_elems[0])
        var namespace = ice_candidate_elems[0].namespaceURI
        if(namespace === ICE_NAMESPACE)
        {
            remoteJID = from
            var sdpMidText = ice_candidate_elems[0].getAttribute(sdpMidText_ATTR_NAME);
            var sdpMLineIndex = parseInt(ice_candidate_elems[0].getAttribute(sdpMLineIndexText_ATTR_NAME));
            var sdpText = Strophe.getText(ice_candidate_elems[0]); 
            // log(sdpMidText);
            // log(sdpMLineIndex);
            // log(sdpText);
            let remote_candidate = new RTCIceCandidate({sdpMid: sdpMidText, 
                sdpMLineIndex: sdpMLineIndex, candidate: sdpText});
            //log(remote_candidate);
            handleNewICECandidateMsg(remote_candidate);
        }
        

    }

    return true;
}


$(document).ready(function () {
    connection = new Strophe.Connection(WS_SERVICE, {'keepalive': true});

    // Uncomment the following lines to spy on the wire traffic.
    connection.rawInput  = function (data) { console.log('RECV: ' + data); };
    connection.rawOutput = function (data) { console.log('SEND: ' + data); };

    // Uncomment the following line to see all the debug output.
    //Strophe.log = function (level, msg) { log('LOG: ' + msg); };


  $('#connect').bind('click', function () {
	  var button = $('#connect').get(0);
    localJID = $('#jid').get(0).value + '@' + DOMAIN_NAME + '/sjs'
    localPass = $('#pass').get(0).value
    remoteJID = $('#remoteJid').get(0).value + '@' + DOMAIN_NAME

	if (button.value == 'Connect') {
	    button.value = 'Disconnect';
      disconnectIsClicked = false;
	    connection.connect(localJID,
			       localPass,
			       onConnect);
	} else {
	    button.value = 'Connect';
	    connection.disconnect();
      disconnectIsClicked = true;
	}
    });

    $('#call-button').bind('click', function () {
        remoteJID = $('#remoteJid').get(0).value + '@' + DOMAIN_NAME
        send_video_invitation();
    });

    $('#hangup-button').bind('click', function () {
        let r = create_video_end(remoteJID, localJID);
        connection.send(r.tree());
        hangUpCall();
    });

    $('#clean-button').bind('click', function () {
      $("#log").html("");
    });

    $('#screen').bind('click', function(){
      if (document.getElementById('screen').checked) {
    
        navigator.mediaDevices.getDisplayMedia(mediaConstraints)
        .then(function(localStream) {
          webcamStream = localStream;
          document.getElementById("local_video").srcObject = webcamStream;
        })
        .catch(handleGetUserMediaError);
      } 
      else{
        localVideo = document.getElementById("local_video");
        if (localVideo.srcObject) {
          localVideo.pause();
          localVideo.srcObject.getTracks().forEach(track => {
            track.stop();
          });
        }
      }
    });
});

setInterval(function(){ 
  var button = $('#connect').get(0);
	if (connection_status == "disconnected" && !disconnectIsClicked)
	{
		//alert("Disconnected"); 
		connection.connect($('#jid').get(0).value + '@' + DOMAIN_NAME + '/sjs',
			       $('#pass').get(0).value,
			       onConnect);
		button.value = 'Disconnect';
	}
	
}, 3000);


async function handleNewICECandidateMsg(remote_candidate) {
    
    log("*** Adding received ICE candidate: " + remote_candidate.candidate);
    console.log("*** Adding received ICE candidate: ")
    console.log(remote_candidate);
    try {
      await myPeerConnection.addIceCandidate(remote_candidate)
    } catch(err) {
      reportError(err);
    }
  }


function send_video_invitation()
{
    //alert("Send Video Offer"); 
    let r = create_video_invitation(remoteJID, localJID);
    connection.send(r.tree());
}

// Create the RTCPeerConnection which knows how to talk to our
// selected STUN/TURN server and then uses getUserMedia() to find
// our camera and microphone and add that stream to the connection for
// use in our video call. Then we configure event handlers to get
// needed notifications on the call.

async function createPeerConnection() {
    log("Setting up a connection...");
  
    // Create an RTCPeerConnection which knows to use our chosen
    // STUN server.
  
    myPeerConnection = new RTCPeerConnection({
      iceServers: [     // Information about ICE servers - Use your own!
        {
          urls: "turn:coturn.fconnect.com.vn",  // A TURN server
          username: "anhnt232",
          credential: "anhnt232",
          urls: "stun:coturn.fconnect.com.vn"
        }
      ]
    });
  
    // Set up event handlers for the ICE negotiation process.
  
    myPeerConnection.onicecandidate = handleICECandidateEvent;
    myPeerConnection.oniceconnectionstatechange = handleICEConnectionStateChangeEvent;
    myPeerConnection.onicegatheringstatechange = handleICEGatheringStateChangeEvent;
    myPeerConnection.onsignalingstatechange = handleSignalingStateChangeEvent;
    myPeerConnection.onnegotiationneeded = handleNegotiationNeededEvent;
    myPeerConnection.ontrack = handleTrackEvent;
  }
  
  // Called by the WebRTC layer to let us know when it's time to
  // begin, resume, or restart ICE negotiation.
  
  async function handleNegotiationNeededEvent() {
    log("*** Negotiation needed");
    if(isCaller)
    {
      try {
        log("---> Creating offer");
        const offer = await myPeerConnection.createOffer();

        // If the connection hasn't yet achieved the "stable" state,
        // return to the caller. Another negotiationneeded event
        // will be fired when the state stabilizes.

        if (myPeerConnection.signalingState != "stable") {
          log("     -- The connection isn't stable yet; postponing...")
          return;
        }

        // Establish the offer as the local peer's current
        // description.

        log("---> Setting local description to the offer");
        await myPeerConnection.setLocalDescription(offer);

        // Send the offer to the remote peer.

        log("---> Sending the offer to the remote peer");

        let r = create_sdp_offer(remoteJID, localJID, myPeerConnection.localDescription.sdp);
        connection.send(r.tree());

      } catch(err) {
        log("*** The following error occurred while handling the negotiationneeded event:");
        reportError(err);
      };
    }
  
  }
  
  // Called by the WebRTC layer when events occur on the media tracks
  // on our WebRTC call. This includes when streams are added to and
  // removed from the call.
  //
  // track events include the following fields:
  //
  // RTCRtpReceiver       receiver
  // MediaStreamTrack     track
  // MediaStream[]        streams
  // RTCRtpTransceiver    transceiver
  //
  // In our case, we're just taking the first stream found and attaching
  // it to the <video> element for incoming media.
  
  function handleTrackEvent(event) {
    log("*** Track event");
    console.log("*** Track event");
    console.log(event.streams);
    document.getElementById("received_video").srcObject = event.streams[0];
    document.getElementById("hangup-button").disabled = false;
  }
  
  // Handles |icecandidate| events by forwarding the specified
  // ICE candidate (created by our local ICE agent) to the other
  // peer through the signaling server.
  
  function handleICECandidateEvent(event) {
    if (event.candidate) {
      log("*** Outgoing ICE candidate: " + event.candidate.candidate);
  
      let r = create_ice_candidate(remoteJID, localJID, event.candidate)
      connection.send(r.tree());

    }
  }
  
  // Handle |iceconnectionstatechange| events. This will detect
  // when the ICE connection is closed, failed, or disconnected.
  //
  // This is called when the state of the ICE agent changes.
  
  function handleICEConnectionStateChangeEvent(event) {
    log("*** ICE connection state changed to " + myPeerConnection.iceConnectionState);
  
    switch(myPeerConnection.iceConnectionState) {
      case "closed":
      case "failed":
      case "disconnected":
        closeVideoCall();
        break;
    }
  }
  
  // Set up a |signalingstatechange| event handler. This will detect when
  // the signaling connection is closed.
  //
  // NOTE: This will actually move to the new RTCPeerConnectionState enum
  // returned in the property RTCPeerConnection.connectionState when
  // browsers catch up with the latest version of the specification!
  
  function handleSignalingStateChangeEvent(event) {
    log("*** WebRTC signaling state changed to: " + myPeerConnection.signalingState);
    switch(myPeerConnection.signalingState) {
      case "closed":
        closeVideoCall();
        break;
    }
  }
  
  // Handle the |icegatheringstatechange| event. This lets us know what the
  // ICE engine is currently working on: "new" means no networking has happened
  // yet, "gathering" means the ICE engine is currently gathering candidates,
  // and "complete" means gathering is complete. Note that the engine can
  // alternate between "gathering" and "complete" repeatedly as needs and
  // circumstances change.
  //
  // We don't need to do anything when this happens, but we log it to the
  // console so you can see what's going on when playing with the sample.
  
  function handleICEGatheringStateChangeEvent(event) {
    log("*** ICE gathering state changed to: " + myPeerConnection.iceGatheringState);
  }



// Close the RTCPeerConnection and reset variables so that the user can
// make or receive another call if they wish. This is called both
// when the user hangs up, the other user hangs up, or if a connection
// failure is detected.

function closeVideoCall() {
  var localVideo = document.getElementById("local_video");

  log("Closing the call");

  // Close the RTCPeerConnection

  if (myPeerConnection) {
    log("--> Closing the peer connection");

    // Disconnect all our event listeners; we don't want stray events
    // to interfere with the hangup while it's ongoing.

    myPeerConnection.ontrack = null;
    myPeerConnection.onnicecandidate = null;
    myPeerConnection.oniceconnectionstatechange = null;
    myPeerConnection.onsignalingstatechange = null;
    myPeerConnection.onicegatheringstatechange = null;
    myPeerConnection.onnotificationneeded = null;

    // Stop all transceivers on the connection

    myPeerConnection.getTransceivers().forEach(transceiver => {
      transceiver.stop();
    });

    // Stop the webcam preview as well by pausing the <video>
    // element, then stopping each of the getUserMedia() tracks
    // on it.

    if (localVideo.srcObject) {
      localVideo.pause();
      localVideo.srcObject.getTracks().forEach(track => {
        track.stop();
      });
    }

    // Close the peer connection

    myPeerConnection.close();
    myPeerConnection = null;
    webcamStream = null;
  }

  // Disable the hangup button

  document.getElementById("hangup-button").disabled = true;
}



// Hang up the call by closing our end of the connection, then
// sending a "hang-up" message to the other peer (keep in mind that
// the signaling is done on a different connection). This notifies
// the other peer that the connection should be terminated and the UI
// returned to the "no call in progress" state.

function hangUpCall() {
    closeVideoCall();

}


// Accept an offer to video chat. We configure our local settings,
// create our RTCPeerConnection, get and attach our local camera
// stream, then create and send an answer to the caller.

async function handleVideoOfferMsg(to, from, sdp_desc) {

  // If we're not already connected, create an RTCPeerConnection
  // to be linked to the caller.

  log("Received video chat offer from " + remoteJID);
  if (!myPeerConnection) {
    createPeerConnection();
  }

  // We need to set the remote description to the received SDP offer
  // so that our local WebRTC layer knows how to talk to the caller.

  var desc = new RTCSessionDescription({type:"offer", sdp:sdp_desc});

  // If the connection isn't stable yet, wait for it...

  if (myPeerConnection.signalingState != "stable") {
    log("  - But the signaling state isn't stable, so triggering rollback");

    // Set the local and remove descriptions for rollback; don't proceed
    // until both return.
    await Promise.all([
      myPeerConnection.setLocalDescription({type: "rollback"}),
      myPeerConnection.setRemoteDescription(desc)
    ]);
    return;
  } else {
    log ("  - Setting remote description");
    await myPeerConnection.setRemoteDescription(desc);
  }

  // Get the webcam stream if we don't already have it

  if (!webcamStream) {
    try {
        webcamStream = await navigator.mediaDevices.getUserMedia(mediaConstraints);
    } catch(err) {
      handleGetUserMediaError(err);
      return;
    }

    document.getElementById("local_video").srcObject = webcamStream;

    // Add the camera stream to the RTCPeerConnection

    try {
      webcamStream.getTracks().forEach(
        transceiver = track => myPeerConnection.addTransceiver(track, {streams: [webcamStream]})
      );
    } catch(err) {
      handleGetUserMediaError(err);
    }
  }

  log("---> Creating and sending answer to caller");

  await myPeerConnection.setLocalDescription(await myPeerConnection.createAnswer());

  let r = create_sdp_answer(to, from, myPeerConnection.localDescription.sdp);
  connection.send(r.tree());

}

// Responds to the "video-answer" message sent to the caller
// once the callee has decided to accept our request to talk.

async function handleVideoAnswerMsg(to, from, sdp_desc) {
    log("*** Call recipient has accepted our call");
  
    // Configure the remote description, which is the SDP payload
    // in our "video-answer" message.
  
    var desc = new RTCSessionDescription({type:"answer", sdp:sdp_desc});
    await myPeerConnection.setRemoteDescription(desc).catch(reportError);
}
  
  
  // Handle errors which occur when trying to access the local media
  // hardware; that is, exceptions thrown by getUserMedia(). The two most
  // likely scenarios are that the user has no camera and/or microphone
  // or that they declined to share their equipment when prompted. If
  // they simply opted not to share their media, that's not really an
  // error, so we won't present a message in that situation.
  
  function handleGetUserMediaError(e) {
    log_error(e);
    switch(e.name) {
      case "NotFoundError":
        alert("Unable to open your call because no camera and/or microphone" +
              "were found.");
        break;
      case "SecurityError":
      case "PermissionDeniedError":
        // Do nothing; this is the same as the user canceling the call.
        break;
      default:
        alert("Error opening your camera and/or microphone: " + e.message);
        break;
    }
  
    // Make sure we shut down our end of the RTCPeerConnection so we're
    // ready to try again.
  
    closeVideoCall();
  }
  
  // Handles reporting errors. Currently, we just dump stuff to console but
  // in a real-world application, an appropriate (and user-friendly)
  // error message should be displayed.
  
  function reportError(errMessage) {
    log_error(`Error ${errMessage.name}: ${errMessage.message}`);
  }

